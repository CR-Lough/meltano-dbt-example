ARG MELTANO_IMAGE=meltano/meltano:v2.3.0
FROM $MELTANO_IMAGE

LABEL maintainer="Connor Lough"

RUN useradd -r -m -U meltanouser

ENV MELTANO_ENVIRONMENT=dev

WORKDIR /project

COPY --chown=meltanouser:meltanouser core/meltano.yml .

RUN meltano install
RUN chown -R meltanouser:meltanouser /project/.meltano/
COPY --chown=meltanouser:meltanouser . .

# USER meltanouser